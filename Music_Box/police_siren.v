module music(
    speaker
);
	parameter fraq = 440;
	parameter clkdivider = 26600000 / fraq / 2;
	output speaker;

	wire fpga_clock;
	OSCH #(.NOM_FREQ("26.60")) rc_oscillator(.STDBY(1'b0), .OSC(fpga_clock));
		
	reg [22:0] tone;
	always @(posedge fpga_clock) 
		tone <= tone+1;

	wire [6:0] ramp = (tone[22] ? tone[21:15] : ~tone[21:15]);
	wire [14:0] clkdivider_Police = {2'b01, ramp, 6'b000000};

	reg [14:0] counter;
	always @(posedge fpga_clock) 
		if(counter==0) 
			counter <= clkdivider_Police; 
		else 
			counter <= counter-1;

	reg speaker;
	always @(posedge fpga_clock) 
		if(counter==0) 
			speaker <= ~speaker;

endmodule
