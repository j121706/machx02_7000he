module music(
    speaker
);
	parameter freq = 440;
	parameter clkdivider = 26600000 / freq / 2;
	output speaker;

	wire fpga_clock;
	OSCH #(.NOM_FREQ("26.60")) rc_oscillator(.STDBY(1'b0), .OSC(fpga_clock));
		
	reg [23:0] tone;
	always @(posedge fpga_clock) 
		tone <= tone+1;

	reg [14:0] counter;
	always @(posedge fpga_clock) 
		if(counter==0) 
			counter <= (tone[23] ? clkdivider-1 : clkdivider/2-1); 
		else 
			counter <= counter-1;

	reg speaker;
	always @(posedge fpga_clock) 
		if(counter==0) 
			speaker <= ~speaker;
        
endmodule
