module music(
    speaker
);
	output speaker;

	wire fpga_clock;
	OSCH #(.NOM_FREQ("26.60")) rc_oscillator(.STDBY(1'b0), .OSC(fpga_clock));
	
	reg [27:0] tone;
	always @(posedge fpga_clock) 
		tone <= tone+1;

	wire [6:0] fastsweep = (tone[22] ? tone[21:15] : ~tone[21:15]);
	wire [6:0] slowsweep = (tone[25] ? tone[24:18] : ~tone[24:18]);
	wire [14:0] clkdivider_pursuit = {2'b01, (tone[27] ? slowsweep : fastsweep), 6'b000000};

	reg [14:0] counter;
	always @(posedge fpga_clock) 
		if(counter==0) 
			counter <= clkdivider_pursuit; 
		else 
			counter <= counter-1;

	reg speaker;
	always @(posedge fpga_clock) 
		if(counter==0) 
			speaker <= ~speaker;

endmodule
