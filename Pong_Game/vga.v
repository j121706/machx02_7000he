// For the standard 640x480 VGA video signal, the frequencies of the pulses should be:

// Vertical Freq (VS)	           ||   Horizontal Freq (HS)
// 60 Hz (=60 pulses per second)   ||   31.5 kHz (=31500 pulses per second)
module monitor (
    R, G, B, vga_h_sync, vga_v_sync
);
    output R, G, B, vga_h_sync, vga_v_sync;

	wire fpga_clock;
	OSCH #(.NOM_FREQ("24.18")) rc_oscillator(.STDBY(1'b0), .OSC(fpga_clock));

    reg [9:0] CounterX;
    reg [8:0] CounterY;
    wire CounterXmaxed = (CounterX==767);

    always @(posedge fpga_clock)
        if(CounterXmaxed)
        CounterX <= 0;
        else
        CounterX <= CounterX + 1;

    always @(posedge fpga_clock)
        if(CounterXmaxed)
            CounterY <= CounterY + 1;

    reg vga_HS, vga_VS;
    always @(posedge fpga_clock)
        begin
            vga_HS <= (CounterX[9:4]==0);   // active for 16 clocks
            vga_VS <= (CounterY==0);   // active for 768 clocks
        end
    
    assign vga_h_sync = ~vga_HS;
    assign vga_v_sync = ~vga_VS;

    assign R = CounterY[3] | (CounterX==256);
    assign G = (CounterX[5] ^ CounterX[6]) | (CounterX==256);
    assign B = CounterX[4] | (CounterX==256);

endmodule