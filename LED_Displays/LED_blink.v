module LED_blink (
    LED
);
    output [4:0] LED; 
        
    wire fpga_clock;
    OSCH #(.NOM_FREQ("7.00")) rc_oscillator(.STDBY(1'b0), .OSC(fpga_clock));

    // binary counter
    reg [23:0] cnt;
    always @(posedge fpga_clock) begin
        cnt <= cnt + 1;
    end

    assign LED[0] = cnt[22];
    assign LED[1] = cnt[22];
    assign LED[2] = cnt[22];
    assign LED[3] = cnt[22];
    assign LED[4] = cnt[22];
    
endmodule