// -----此實驗目前無解...-----
// * issue of https://www.fpga4fun.com/Opto3.html:
//      1. always @(*)的寫法在我的板子上不會動，一定要使用上升緣 -> always @(posedge fpga_clock)
//      2. wire cntovf = &cnt;  寫法挺詭異，找不到&可以這樣用的...，但它的確會動
//      3. Switch在 LED 的情況十分詭異，內部變數會正常往上加，但反映到LED卻都不是預期情況(教學code就已經沒有反應了...)
//      4. 最後已經精簡到下方的形狀了，仍然無法正常顯示

module LED_7seg (
    segA, segB, segC
);
    output segA, segB, segC;
    reg segA, segB, segC;
    // assign {segA, segB, segC} = 3'b101;

    wire fpga_clock;
    OSCH #(.NOM_FREQ("20.46")) rc_oscillator(.STDBY(1'b0), .OSC(fpga_clock));

    // 24bit counter
    reg [23:0] cnt;  // we use a 24bit counter
    always @(posedge fpga_clock) 
        cnt <= cnt + 24'h1;  // to count up to 16 million

    reg [2:0] BCD;  // BCD is a counter
    always @(posedge fpga_clock)
        if(cnt[23]) // that increments every second
            BCD <= BCD + 1;  // from 0 to 9
            // {segA, segB, segC} = ~{segA, segB, segC};

    // @@...
    always @(posedge fpga_clock)
        case(BCD)
            0: begin {segA, segB, segC} = 3'b110;  end
            1: begin {segA, segB, segC} = 3'b101;  end
            2: begin {segA, segB, segC} = 3'b011;  end
            3: begin {segA, segB, segC} = 3'b111;  end
            default: begin {segA, segB, segC} = 3'b111;  end
        endcase

endmodule