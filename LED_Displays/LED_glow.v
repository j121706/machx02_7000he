module LEDglow (
    LED
);
    output LED; 
        
    wire fpga_clock;
    OSCH #(.NOM_FREQ("26.60")) rc_oscillator(.STDBY(1'b0), .OSC(fpga_clock));

    reg [23:0] cnt;
    always @(posedge fpga_clock) begin
        cnt <= cnt + 1;
    end

    reg [4:0] PWM;
    wire [3:0] intensity = cnt[23] ? cnt[22:19] : ~cnt[22:19];
    always @(posedge fpga_clock) begin
        PWM <= PWM[3:0] + intensity;
    end

    assign LED = PWM[4];
    
endmodule