module LED_blink (
    LED
);
    output LED; 
        
    wire fpga_clock;
    OSCH #(.NOM_FREQ("7.00")) rc_oscillator(.STDBY(1'b0), .OSC(fpga_clock));

    // binary counter\
    reg toggle;
    always @(posedge fpga_clock) begin
        toggle <= ~toggle;
    end

    assign LED = toggle;
    
endmodule